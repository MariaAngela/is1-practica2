package domain;

import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "Sede", indexes = { @Index(columnList = "codigo") })
public class Sede implements BaseEntity<Long> {

	@Id
	@SequenceGenerator(name = "sede_id_generator", sequenceName = "sede_id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sede_id_generator")
	private Long id;

	@Column(unique = true, nullable = false, updatable = false, length = 64)
	private String direccion;

	@Column(nullable = false)
	private double codigo;

	@OneToMany(mappedBy = "Banco", fetch = FetchType.LAZY)
	private Collection<Operation> Sede;


	public Sede() {
	}

	public Sede(String direccion, int codigo) {
		this.direccion = direccion;
		this.codigo = codigo;
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public int getCodigo() {
		return codigo;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

